<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\facade\Route;

Route::get('sitemap_:page.xml', 'index/build_site_index_map');
Route::get('sitemap.xml', 'index/build_site_map');

//章节详情
Route::get('chapter/:primary_id/:chapter_id', 'chapter/index');

//小说详情
Route::get('book/:primary_id', 'article/index');
//移动端章节列表
Route::get('book_list/:id', 'article/book_list');
//作者
Route::get('author/:name', 'article/author');
//移动端
Route::get('top', 'index/top');
Route::get('sort', 'category/sort');
Route::get('history', 'index/history');
//书架
Route::get('bookcase', 'user/bookcase');

//全本
Route::get('full_:page', 'index/full');
Route::get('full', 'index/full');

Route::get('search', 'index/search');
//最近更新
Route::get('last_insert', 'index/last_insert');
Route::get('update_cache', 'article/update_cache');

Route::get('desktop', 'index/desktop');

return [];
