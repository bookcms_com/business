layui.define(function(exports){
	var o = layui.jquery;
	
	var MOD_NAME = 'multiple',selected = [];
	
	// 此处可传值初始化参数扩展
	var mul = function() {};
	
    // 模板数据重载
    mul.prototype.template = function(){
    	var e = this;
    	// 点击下拉内容选择
    	o('select[multiple]').next('.layui-unselect').find('.layui-anim dd').click(function(){
    		var t = o(this).parents('.layui-unselect'),select = t.prev('select[multiple]'),index = o('select[multiple]').index(select);
    		var val = o(this).attr('lay-value'),text = o(this).text();
    		// 点击选项为空值或者此项数组不存在怎初始化
    		if(val === '' || !selected[index]){
    			selected[index] = [];
    		}else{
		    	// 选值选中或取消存于数组中
		    	selected[index][val] ? delete selected[index][val] : selected[index][val] = text;
    		}
	    	// 初始化为清除选中
    		t.find('dd').removeClass('layui-this');
    		// 初始化清除dom选项
    		select.find('option').prop('selected',false);
    		// 小图片打开状态不改动
    		o(this).parents('.layui-unselect').addClass('layui-form-selected');
    		e.selected(select,selected[index]);
    	})
    	
    	// 初始化多选效果
    	o('select[multiple]').each(function(){
    		var index = o('select[multiple]').index(o(this));
    		if(!selected[index]) selected[index] = [];
    		// 默认选中处理
    		o(this).find('option:selected').each(function(){
    			selected[index][o(this).val()] = o(this).text();
    		})
    		o(this).next('.layui-unselect').find('.layui-anim').css('display','none');
    		e.selected(o(this),selected[index]);
    	})
    	
    	// 页面的点击效果（主要处理下拉框的显隐效果）
	    o(document).click(function(event){
	    	var unselect = o(event.target).parents('.layui-unselect'),isHide = unselect.find('.layui-anim').css('display') == 'none';
	    	o('select[multiple]').next('.layui-unselect').removeClass('layui-form-selected').find('.layui-anim').css('display','none');
	    	if(unselect && unselect.prev('select[multiple]').length){
	    		// 下拉框显示隐藏转换
				unselect.find('.layui-anim').css('display',isHide ? 'block' : 'none');
				// 是否下拉小图标显示状态
				isHide ? unselect.addClass('layui-form-selected') : unselect.removeClass('layui-form-selected');
				// 保证点击选择的时候数据不被清掉，重新渲染一下
				var index = o('select[multiple]').index(unselect.prev('select[multiple]'));
				e.selected(unselect.prev('select[multiple]'),selected[index]);
			}
	    })
    }

    
    // 选择项处理
    mul.prototype.selected = function(select,arr){
    	var t = select.next('.layui-unselect'),values = '';
    	for(var val in arr){
    		// 选中项给选中状态
    		t.find('dd[lay-value='+val+']').addClass('layui-this');
    		values += ','+arr[val];
    		select.find('option[value='+val+']').prop('selected',true);
    	}
    	// 显示框值显示
    	t.find('.layui-select-title').find('input').val(values.substr(1));
    }
	
	var mul = new mul();mul.template();
	
	exports(MOD_NAME,mul);
});