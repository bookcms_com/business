<?php
/**
 * Created by PhpStorm.
 * User: bookcms
 * Date: 2018/7/11
 * Time: 下午8:56
 */

namespace app\common\taglib;

use think\Db;
use think\facade\Config;
use think\facade\Url;
use think\template\TagLib;

class Tag extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'web' => ['attr' => 'name,value', 'close' => 0],
        'ad' => ['attr' => 'alias', 'close' => 0],
        'novel_list' => ['attr' => 'cid,limit', 'close' => 1],
        'block_list' => ['attr' => 'block_id,limit', 'close' => 1],
    ];

    public function tagAd($tag, $content) {
        $alias = isset($tag['alias']) ? $tag['alias'] : '';

        //分类id
        if (!empty($alias)) {
            $alias  = $this->autoBuildVar($alias);
        }

        $parse = <<<EOF
        <?php
                \$content = get_ad('$alias');
                echo \$content;
            ?>
EOF;
        return $parse;
    }

    public function tagNovel_list($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : 10;
        $offset = isset($tag['offset']) ? $tag['offset'] : 0;
        $cid = isset($tag['cid']) ? $tag['cid'] : '';

        //分类id
        if (!empty($cid)) {
            $cid  = $this->autoBuildVar($cid);
        }

        $parse = <<<EOF
        <?php
           
           \$__LIST__ = \model('article')->where('Cid','eq',$cid)->limit($offset,$limit)->order('PrimaryId','desc')->cache()->select();
           if(!empty(\$__LIST__)):
                foreach(\$__LIST__ as \$key => \$novel):
            ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        $parse .= '<?php endif; ?>';
        return $parse;
    }

    public function tagBlock_list($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : 10;
        $block_id = isset($tag['block_id']) ? $tag['block_id'] : '';

        if (!empty($block_id)) {
            $block_id  = $this->autoBuildVar($block_id);
        }

        $parse = <<<EOF
        <?php
                \$__LIST__ = get_block_list($block_id,$limit);
                if(is_array(\$__LIST__)):
                   foreach(\$__LIST__ as \$key => \$novel):
            ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        $parse .= '<?php endif; ?>';

        return $parse;
    }


}