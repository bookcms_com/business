<?php

namespace app\common\controller;

use think\Controller;
use think\Exception;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Env;
use think\facade\Log;
use think\facade\Request;

class Common extends Controller
{
    protected $site_config = null;
    protected $current_cate = null;

    protected function initialize()
    {
        parent::initialize();
        $this->site_config = get_site_config();
        $this->initRouteConfig();

        $this->assign("site_config", $this->site_config);

        $this->current_cate = array(
            'name' => "首页",
            'alias' => "home",
        );

        $paths = explode("/",$this->request->path());

        if (is_array($this->site_config['category_list']) && count($paths) > 0) {
            foreach ($this->site_config['category_list'] as $category) {
                if (strpos($paths[0],$category['alias']) !== false) {
                    $this->current_cate = $category;
                    break;
                }
            }
        }

//        $runtime_path = Env::get('runtime_path');
//        if ( delete_dir_file(	$runtime_path . 'temp/') ) {
//            echo "del";
//        }
        $this->assign("current_cate",$this->current_cate);
    }

    private function initRouteConfig()
    {
        $site_config = get_site_config();
        if (!empty($site_config['category_list'])) {

            $routePath = Env::get('root_path') . 'route/index.php';
            if (!is_file($routePath)) {
                $route = '';
                if (is_array($site_config['category_list'])) {

                    foreach ($site_config['category_list'] as $item) {
                        $route .= "Route::get('{$item['alias']}_:page', 'category/index');\r\nRoute::get('{$item['alias']}', 'category/index');\r\n\r\n";
                    }

                    $code = <<<INFO
<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\\facade\\Route;

{$route}

INFO;
                    file_put_contents($routePath, $code);
                }
            }
            Log::info('初始化路由文件成功');
        }else {
            Log::info('初始化路由文件失败');
        }
    }

    /**
     * 站点SEO
     * @param string $seo_type 页面类型
     * @param array $param 参数
     * @throws
     */
    protected function site_seo($seo_type = "",$param = array())
    {
        $site_config = get_site_config();

        $site_title = '';
        $keywords = '';
        $description = '';

        foreach ($site_config['site_seo'] as $seo) {

            if ($seo['alias'] == "home" && $seo_type == $seo['alias']) { //首页
                if(!empty($seo['item']['title'])){
                    $site_title = trim(str_replace(array('{site_name}'), array($site_config['site_name']), $seo['item']['title']));
                }
                if(!empty($seo['item']['keywords'])){
                    $keywords = trim(str_replace(array('{site_name}'), array($site_config['site_name']), $seo['item']['keywords']));
                }
                if(!empty($seo['item']['description'])){
                    $description = trim(str_replace(array('{site_name}'), array($site_config['site_name']), $seo['item']['description']));
                }
                break;
            }else if ($seo['alias'] == "details"  && $seo_type == $seo['alias']) { //详情
                $details = $param['details'];

                if(!empty($seo['item']['title'])){
                    $site_title = trim(str_replace(array('{title}','{author}','{category_name}','{site_name}'), array($details['Title'],$details['Author'],$param['category_name'],$site_config['site_name']), $seo['item']['title']));
                }
                if(!empty($seo['item']['keywords'])){
                    $keywords = trim(str_replace(array('{title}','{author}','{category_name}','{site_name}'), array($details['Title'],$details['Author'],$param['category_name'],$site_config['site_name']), $seo['item']['keywords']));
                }
                if(!empty($seo['item']['description'])){
                    $description = trim(str_replace(array('{title}','{author}','{category_name}','{introduction}','{UpdateTime}','{site_name}'), array($details['Title'],$details['Author'],$param['category_name'],$details['Introduction'],date("Y-m-d H:i:s",$details['UpdateTime']),$site_config['site_name']), $seo['item']['description']));
                }
                break;
            } else if ($seo['alias'] == "chapter" && $seo_type == $seo['alias']) { //章节
                $article = $param['article'];
                $chapter = $param['chapter'];

                if(!empty($seo['item']['title'])){
                    $site_title = trim(str_replace(array('{site_name}','{title}','{author}','{category_name}','{chapter_title}'), array($site_config['site_name'],$article['Title'],$article['Author'],$param['category_name'],$chapter['Title']), $seo['item']['title']));
                }
                if(!empty($seo['item']['keywords'])){
                    $keywords = trim(str_replace(array('{site_name}','{title}','{author}','{category_name}','{chapter_title}'), array($site_config['site_name'],$article['Title'],$article['Author'],$param['category_name'],$chapter['Title']), $seo['item']['keywords']));
                }
                if(!empty($seo['item']['description'])){
                    $description = trim(str_replace(array('{site_name}','{title}','{author}','{category_name}','{introduction}','{UpdateTime}','{chapter_title}'), array($site_config['site_name'],$article['Title'],$article['Author'],$param['category_name'],$article['Introduction'],date("Y-m-d H:i:s",$article['UpdateTime']),$chapter['Title']), $seo['item']['description']));
                }
                break;
            }else if ($seo_type == "category" && $seo_type == $seo['alias']) { //分类seo
                $category = $param['category'];

                if(!empty($seo['item']['title'])){
                    $site_title = trim(str_replace(array('{category_name}','{site_name}'), array($category['name'],$site_config['site_name']), $seo['item']['title']));
                }
                if(!empty($seo['item']['keywords'])){
                    $keywords = trim(str_replace(array('{category_name}','{site_name}'), array($category['name'],$site_config['site_name']), $seo['item']['keywords']));
                }
                if(!empty($seo['item']['description'])){
                    $description = trim(str_replace(array('{category_name}','{site_name}'), array($category['name'],$site_config['site_name']), $seo['item']['description']));
                }
                break;
            } else if ($seo_type == 'author' && $seo_type == $seo['alias']){ //作者seo
                if(!empty($seo['item']['title'])){
                    $site_title = trim(str_replace(array('{site_name}','{author}'), array($site_config['site_name'],$param['author']), $seo['item']['title']));
                }
                if(!empty($seo['item']['keywords'])){
                    $keywords = trim(str_replace(array('{site_name}','{author}'), array($site_config['site_name'],$param['author']), $seo['item']['keywords']));
                }
                if(!empty($seo['item']['description'])){
                    $description = trim(str_replace(array('{site_name}','{author}'), array($site_config['site_name'],$param['author']), $seo['item']['description']));
                }
                break;
            } else if ($seo_type == 'search' && $seo_type == $seo['alias']){ //搜索seo
                if(!empty($seo['item']['title'])){
                    $site_title = trim(str_replace(array('{site_name}','{keyword}'), array($site_config['site_name'],$param['keyword']), $seo['item']['title']));
                }
                if(!empty($seo['item']['keywords'])){
                    $keywords = trim(str_replace(array('{site_name}','{keyword}'), array($site_config['site_name'],$param['keyword']), $seo['item']['keywords']));
                }
                if(!empty($seo['item']['description'])){
                    $description = trim(str_replace(array('{site_name}','{keyword}'), array($site_config['site_name'],$param['keyword']), $seo['item']['description']));
                }
                break;
            }

        }
        $site_seo = array(
            'title' =>  $site_title,
            'keywords' =>   $keywords,
            'description' =>   $description,
        );

        $this->assign('site_seo', $site_seo);
    }

    /**
     * 获取模板
     * @param string $template 模板名称
     * @param array $vars 模板参数
     * @param array $config
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $config = [])
    {
        $domain = str_replace(array('https://','http://'),'',Request::domain());
        $domain = explode('.',$domain);
        if (is_array($domain) && $domain[0] == 'm') {
            $template .= '_m';
        }
        return parent::fetch($template, $vars, $config);
    }


    /**
     * 生成静态
     * @param string $htmlFile 文件名称
     * @param string $htmlPath 文件路径
     * @param string $templateFile 模板文件
     * @return bool
     */
    protected function buildHtml($htmlFile='',$htmlPath='',$templateFile='') {
        $content    =   $this->fetch($templateFile);
        $htmlPath   =   !empty($htmlPath) ? $htmlPath : Env::get('root_path') . '/public/'; // 项目目录下的html目录
        $htmlFile   =   $htmlPath.$htmlFile.'.html';
        $dir   =  dirname($htmlFile);
        if(!is_dir($dir)){
            mkdir($dir,0777,true);
        }
        if(file_put_contents($htmlFile,$content) === false) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * 输出json 数据
     * @param array $data
     * @param int $code
     * @param string $msg
     */
    protected function echoJson($data = array(),$code = 200,$msg = 'ok')
    {
        $data = array(
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        header('Content-Type: application/json; charset=utf-8');
        exit(json_encode($data));
    }
}
