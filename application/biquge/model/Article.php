<?php

namespace app\biquge\model;
use think\Model;

class Article extends  Model
{
    protected function initialize()
    {
        parent::initialize();
    }

    public function getCidAttr($value = ''){
        return get_offset_value($value);
    }

    public function getPrimaryIdAttr($value = ''){
        return get_offset_value($value);
    }

}
