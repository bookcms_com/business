<?php
namespace app\biquge\controller;

use app\common\controller\Common;
use think\Exception;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Request;

class User extends Common
{

    protected function initialize()
    {
        parent::initialize();
        $this->assign("current_cate",array('name' => "临时书架", 'alias' => "bookcase",));
    }

    public function bookcase()
    {
        $this->site_seo('user');
        return $this->fetch('index');
    }
}

